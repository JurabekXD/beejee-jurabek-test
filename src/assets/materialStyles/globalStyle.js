import { makeStyles } from '@material-ui/core'


export const globalStyle = makeStyles(theme => ({
  title: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing(1),
  },
}))


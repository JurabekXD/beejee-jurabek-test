import { makeStyles } from '@material-ui/core'
import { teal, red } from '@material-ui/core/colors'


export const snackbarStyles = makeStyles(theme => ({
  success: {
    backgroundColor: teal[600]
  },
  error: {
    backgroundColor: red[900]
  },
  icon: {
    fontSize: 21
  },
  iconVariant: {
    opacity: 0.7,
    marginRight: theme.spacing(1)
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  }
}))


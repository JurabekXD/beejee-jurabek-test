import React, { useContext } from 'react';
import ToDoContext from './todo-context';
import AuthContext from '../auth/auth-context';

import todoTableStyle from '../../assets/materialStyles/todoTableStyle';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button'

// ! Can't use Changed by Admin status since Server doesn't let me change status of tasks to anything but 0 or 10
const defineStatus = (status) => {
  switch (status) {
    case 10:
      return 'Done';
    case 5:
      return 'Changed by admin'
    default:
      return 'In Progress';
  }
}

const ToDoTask = ({ task }) => {
  let { id, username, email, text, status } = task;
  const { logged } = useContext(AuthContext);
  const { handleToDoList, toggleTodoAddFrom, setEditTaskData } = useContext(ToDoContext);

  let textStatus = defineStatus(status);
  return <TableRow key={id}>
    <TableCell>{username}</TableCell>
    <TableCell>{email}</TableCell>
    <TableCell>{text}</TableCell>
    <TableCell>{textStatus}</TableCell>
    {
      logged && (
        <TableCell>
          <Button onClick={() => { toggleTodoAddFrom(true); return setEditTaskData({ ...task }); }}>Изменить</Button>
          <Button onClick={() => handleToDoList({ action: 'modify', payload: { ...task, status: 10 } })}>Выполнено</Button>
        </TableCell>
      )
    }
  </TableRow>
}

const TableHeadList = ({ name, id, classNames }) => {
  const { state: { sort }, getList } = useContext(ToDoContext)
  let { sort_field, sort_direction } = sort;
  return (
    <TableCell
      id={id}
      key={id}
      padding={'default'}
      sortDirection={sort_field === id ? sort_direction : false}
    >
      <TableSortLabel
        active={sort_field === id}
        direction={sort_direction}
        onClick={() => getList({ page: 1, sort_field: id, sort_direction })}
      >
        {name}
      </TableSortLabel>
    </TableCell>
  )
}

const TodoList = () => {
  const { state: { tasks, page, total_task_count }, getList } = useContext(ToDoContext)
  const { logged } = useContext(AuthContext);
  const tableHeads = {
    username: "Имя",
    email: "Email",
    text: "Text",
    status: "Status"
  };
  const classNames = todoTableStyle();
  return (
    <div className={classNames.root}>
      <Paper className={classNames.paper}>
        <Table
          size={'small'}
        >
          <TableHead>
            <TableRow>
              {
                Object.keys(tableHeads).map(id => <TableHeadList key={id} name={tableHeads[id]} id={id} classNames={classNames} />)
              }
              {logged && <TableCell>Действия</TableCell>}
            </TableRow>
          </TableHead>
          <TableBody>
            {
              tasks.map(task => <ToDoTask key={task.id} task={task} />)
            }
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[3]}
                rowsPerPage={3}
                count={total_task_count}
                page={page - 1}
                backIconButtonProps={{
                  'aria-label': 'previous page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'next page',
                }}
                onChangePage={(event, page) => getList({ page: page + 1 })}
              />
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    </div>

  )
}

export default TodoList;
import React, { Component } from 'react'

import TodoContext from './todo-context';
import { SnackbarContext } from '../Snackbar';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class ToDoAddFrom extends Component {
  static contextType = TodoContext;

  constructor(props, context) {
    super(props, context);

    let { editTaskData = { username: '', text: '', email: '', id: '', status: 5 } } = this.context;

    this.state = {
      ...editTaskData
    }
  }

  handleChange = input => e => {
    this.setState({
      [input]: e.target.value
    })
  }

  handleClose = () => {
    let { toggleTodoAddFrom, setEditTaskData } = this.context;
    toggleTodoAddFrom(false);
    return setEditTaskData();
  }

  submitTask = ({ snackBar }) => {
    let { handleToDoList, editTaskData } = this.context;
    let { email, text, username } = this.state;

    let emailRegExp = /^[ ]*([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})[ ]*$/i;
    let validateEmail = emailRegExp.test(email);

    // * Check fields in order to send to server

    if (validateEmail && text && username) {

      let action = editTaskData ? 'modify' : 'add';

      return handleToDoList({ action, payload: this.state })
        .then(({ type, messageText }) => snackBar({ show: true, type, message: messageText }))
        .then(() => this.handleClose())

    } else {
      let errorMsg = "";

      if (!validateEmail) {
        errorMsg = 'Please write correct email';
      } else if (!text) {
        errorMsg = 'Please write description of tasks';
      } else if (!username) {
        errorMsg = 'Please write username';
      }
      return snackBar({ show: true, type: 'error', message: errorMsg });
    }
  }


  render() {
    let {
      showToDoAddForm,
      editTaskData
    } = this.context;
    return (
      <Dialog
        open={showToDoAddForm}
        onClose={this.handleClose}
        area-labeledby='pop-up-todoaddform'
      >
        <DialogTitle>Add a new task to the list</DialogTitle>
        <DialogContent>
          <TextField
            margin="dense"
            id="username"
            label="Username"
            multiline
            onChange={this.handleChange('username')}
            value={this.state.username}
            fullWidth
          />
          <TextField
            margin="dense"
            id="email"
            label="Email"
            type="email"
            multiline
            onChange={this.handleChange('email')}
            value={this.state.email}
            fullWidth
          />
          <TextField
            margin="dense"
            id="text"
            label="Text"
            type="text"
            multiline
            onChange={this.handleChange('text')}
            value={this.state.text}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button color='primary' onClick={this.handleClose}>
            Close
              </Button>
          <SnackbarContext.Consumer>
            {({ setValues }) =>
              <Button
                color="primary"
                onClick={() => this.submitTask({ snackBar: setValues })}
              >
                {editTaskData ? 'Edit' : "Add"}
              </Button>
            }
          </SnackbarContext.Consumer>
        </DialogActions>
      </Dialog>
    )
  }
}

export default ToDoAddFrom

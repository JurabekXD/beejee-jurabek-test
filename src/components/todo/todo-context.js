import React, { useState, useEffect } from 'react';
import axios from 'axios';

const ToDoContext = React.createContext();

export const ToDoProvider = ToDoContext.Provider;
export const ToDoConsumer = ToDoContext.Consumer;

export const ToDoProviderWrapper = (props) => {

  const [state, setList] = useState({
    tasks: [],
    page: 1,
    total_task_count: 0,
    sort: {
      sort_field: "id",
      sort_direction: "asc"
    }
  });
  const [showToDoAddForm, toggleTodoAddFrom] = useState(false);
  const [editTaskData, setEditTaskData] = useState();

  const getList = async ({ page, sort_field = state.sort.sort_field, sort_direction }) => {
    const isDesc = state.sort.sort_field === sort_field && sort_direction === 'desc';
    sort_direction = isDesc ? 'asc' : 'desc';

    let res = await axios
      .get(
        'https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=Jurabek',
        {
          params: {
            page,
            sort_field,
            sort_direction
          }
        }
      )
    return setList({
      tasks: res.data.message.tasks,
      page: page,
      total_task_count: res.data.message.total_task_count * 1,
      sort: {
        sort_field,
        sort_direction
      }
    });
  }

  useEffect(() => {
    getList({ page: 1 })
  }, [])

  const responseMessage = ({ status, message }) => {
    let type = status === 'ok' ? 'success' : 'error';
    let messageText = "Successfully added";

    if (status === 'error') {
      let { username, text, email, token } = message;

      if (username) {
        messageText = username;
      } else if (text) {
        messageText = text;
      } else if (email) {
        messageText = email
      } else if (token) {
        messageText = token;
      }
    }

    return { type, messageText }
  }

  // ! Tried many staskStatuses in order to set a different values rather that 0 or 5, doesn't work
  const handleToDoList = async ({ action, payload }) => {
    let { email, username, id, text, status: taskStatus = 5 } = payload;

    let form = new FormData();

    switch (action) {
      case 'modify': {
        form.append('text', text);
        form.append('status', taskStatus === 10 ? taskStatus : 5);
        form.append('token', localStorage.getItem('token'))

        if (localStorage.getItem('logged') !== 'true' && taskStatus !== 10) {
          return { type: 'error', messageText: "Log in first" }
        }

        let { data: { status, message } } =
          await axios.post(`https://uxcandy.com/~shapoval/test-task-backend/v2/edit/${id}/?developer=Jurabek`, form)

        getList({ page: state.page, sort_field: state.sort.sort_field })

        return responseMessage({ status, message });
      }
      case 'add': {
        form.append('text', text);
        form.append('email', email);
        form.append('username', username)

        let { data: { status, message } } = await axios
          .post(
            'https://uxcandy.com/~shapoval/test-task-backend/v2/create?developer=Jurabek',
            form
          )
        getList({ page: 1, sort_field: 'id', sort_direction: 'asc' })
        return responseMessage({ status, message });
      }
      default: {
        return action;
      }
    }

  }

  return (
    <ToDoProvider
      value={{
        state,
        editTaskData,
        setEditTaskData,
        handleToDoList,
        showToDoAddForm,
        toggleTodoAddFrom,
        getList
      }}
    >
      {props.children}
    </ToDoProvider>
  )
}

export default ToDoContext;
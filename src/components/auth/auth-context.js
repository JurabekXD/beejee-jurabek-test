import React, { useState, useEffect } from 'react';
import axios from 'axios';

const AuthContext = React.createContext();

export const AuthProvider = AuthContext.Provider;
export const AuthConsumer = AuthContext.Consumer;

export const AuthProviderWrapper = (props) => {
  const [logged, setLogged] = useState(
    localStorage.getItem('logged') === 'true'
  );
  const [showForm, toggleForm] = useState(false);

  useEffect(() => {
    localStorage.setItem('logged', logged)
  }, [logged])

  const logIn = async ({ form }) => {

    let res = await axios.post(
      'https://uxcandy.com/~shapoval/test-task-backend/v2/login?developer=Name',
      form
    )

    let { status, message: { username, password, token } } = res.data;

    let error = username ? username : password;
    if (status === 'ok') {
      toggleForm(false);
      localStorage.setItem('token', token);
      setLogged(true);
    }
    return {
      status: status === 'ok' ? 'success' : 'error',
      message: error ? error : 'Logged In! Welcome!!'
    }
  }

  return (
    <AuthProvider
      value={{
        logged,
        logIn: logIn,
        logOut: () => { localStorage.removeItem('token'); return setLogged(false) },
        showForm,
        toggleForm
      }}
    >
      {props.children}
    </AuthProvider>
  )

}

export default AuthContext;
import React, { Component } from 'react'

import { AuthConsumer } from './auth-context';
import { SnackbarContext } from '../Snackbar'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'

export class AuthForm extends Component {
  state = {
    username: '',
    password: ''
  }

  handleChange = input => e => {
    this.setState({
      [input]: e.target.value
    })
  }

  submitForm = ({ logIn }) => {
    let form = new FormData();
    form.append('username', this.state.username);
    form.append('password', this.state.password);
    return logIn({ form })

  }
  
  render() {
    return (
      <AuthConsumer>
        {({ logIn, showForm, toggleForm }) =>
          <Dialog open={showForm} onClose={() => toggleForm(false)} aria-labelledby="login-form">
            <DialogTitle>
              Login Form
            </DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="username"
                label="Username"
                type="text"
                multiline
                onChange={this.handleChange('username')}
                fullWidth
              />
              <TextField
                margin="dense"
                id="password"
                label="Password"
                type="password"
                multiline
                onChange={this.handleChange('password')}
                fullWidth
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={() => toggleForm(false)} color='primary'>Close</Button>
              <SnackbarContext.Consumer>
                {({ setValues }) =>
                  <Button
                    color='primary'
                    onClick={() => this.submitForm({ logIn }).then((resolve) => setValues({ show: true, type: resolve.status, message: resolve.message }))
                    }
                  >
                    Login
                    </Button>
                }
              </SnackbarContext.Consumer>
            </DialogActions>
          </Dialog>
        }
      </AuthConsumer>
    )
  }
}

/* 
  
*/

export default AuthForm

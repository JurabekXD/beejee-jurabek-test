import React, { useState, createContext, useContext } from 'react';

import SnackBar from '@material-ui/core/SnackBar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import IconButton from '@material-ui/core/IconButton';

import { snackbarStyles } from '../assets/materialStyles/snackbarStyles';


const iconHolder = {
  error: ErrorIcon,
  success: CheckCircleIcon
}
export const SnackbarContext = createContext();

const SnackBarContentWrapper = (props) => {
  const classes = snackbarStyles();
  const { vals, setValues } = useContext(SnackbarContext);
  const Icon = iconHolder[vals.type];

  return (
    <SnackbarContent
      className={classes[vals.type]}
      aria-describedby="snackbar-content"
      message={
        <span className={classes.message}>
          <Icon className={`${classes.icon} ${classes.iconVariant}`} />
          {vals.message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="close-snackbar-content"
          color="inherit"
          onClick={() => setValues({ show: false, type: vals.type, message: vals.message })}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>
      ]}
      {...props}
    />
  )

}




export const Snackbar = (props) => {
  const [values, setValues] = useState({ show: false, type: 'success', message: 'Done' })
  return (
    <>
      <SnackbarContext.Provider
        value={{
          vals: values,
          setValues: setValues
        }}
      >
        {props.children}
        <SnackBar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          open={values.show}
          autoHideDuration={4000}
          onClose={() => setValues({ show: false, type: values.type, message: values.message })}
        >
          <SnackBarContentWrapper />
        </SnackBar>
      </SnackbarContext.Provider>
    </>
  )
}
import React, { useContext } from 'react'

import { AppBar, Typography, Button } from '@material-ui/core';
import ToolBar from '@material-ui/core/ToolBar';

import { globalStyle } from '../assets/materialStyles/globalStyle';

import AuthContext from './auth/auth-context';
import ToDoContext from './todo/todo-context';

export const MenuBar = () => {
  const classNames = globalStyle();
  const { logged, logOut, toggleForm } = useContext(AuthContext)
  const { showToDoAddForm, toggleTodoAddFrom } = useContext(ToDoContext);
  
  return (
    <AppBar position='static' title={"Todo"}>
      <ToolBar>
        <Typography className={classNames.title} variant='h6'>
          ToDo List
          </Typography>
        <Button variant="contained" className={classNames.button} onClick={() => toggleTodoAddFrom(!showToDoAddForm)} >
          Add Task
        </Button>
        {
          !logged ?
            <Button variant="contained" onClick={() => toggleForm(true)} className={classNames.button} >
              Login
            </Button> :
            <Button variant="contained" onClick={() => logOut()} className={classNames.button} >
              LogOut
            </Button>
        }
      </ToolBar>
    </AppBar>
  )
}
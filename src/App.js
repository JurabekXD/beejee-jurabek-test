import React from 'react';
import './App.css';

import AuthForm from './components/auth/AuthForm';
import { MenuBar } from './components/MenuBar';
import TodoList from './components/todo/TodoList.js';
import ToDoAddForm from './components/todo/ToDoAddFrom';
import { ToDoProviderWrapper, ToDoConsumer } from './components/todo/todo-context';
import { AuthProviderWrapper, AuthConsumer } from './components/auth/auth-context'
import { Snackbar as SnackBarWrapper } from './components/Snackbar'

const App = () => {
  return (
    <SnackBarWrapper>
      <AuthProviderWrapper>
        <ToDoProviderWrapper>
          <MenuBar />
          <AuthConsumer>
            {({ showForm }) => showForm && <AuthForm />}
          </AuthConsumer>
          <ToDoConsumer>
            {({ showToDoAddForm }) => showToDoAddForm && <ToDoAddForm />}
          </ToDoConsumer>
          <TodoList />
        </ToDoProviderWrapper>
      </AuthProviderWrapper>
    </SnackBarWrapper>

  )
}

export default App;
